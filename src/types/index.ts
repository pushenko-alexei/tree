export interface ITreeItem {
    id: number;
    parentId: number | null;
    name: string;
    checked: boolean;
    children?: ITreeItem[]
}