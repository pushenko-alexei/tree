export default function listToTree(data: any[], options: any) {
    const ID_KEY = options.idKey;
    const PARENT_KEY = options.parentKey;
    const CHILDREN_KEY = options.childrenKey;
  
    const tree = [],
      childrenOf: any = {};
    let item, id, parentId;
  
    for (let i = 0, length = data.length; i < length; i++) {
      item = data[i];
      id = item[ID_KEY];
      parentId = item[PARENT_KEY] || 0;
      childrenOf[id] = childrenOf[id] || [];
      item[CHILDREN_KEY] = childrenOf[id];
      if (parentId != 0) {
        childrenOf[parentId] = childrenOf[parentId] || [];
        childrenOf[parentId].push(item);
      } else {
        tree.push(item);
      }
    }
  
    return tree;
  }
  