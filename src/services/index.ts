import Data from "./data.json"
import {ITreeItem} from "@/types"
class ApiRequest{
    /**
     * getData
     */
    public getData(): Promise<ITreeItem[]> {
        return new Promise((resolve, reject) => {
            if(Data.data.length){
                resolve(Data.data)
            }else{
                reject("Извините, что то пошло не так")
            }
        })
    }
}

const api = new ApiRequest()

export {api}